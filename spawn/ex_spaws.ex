defmodule Processes do
  def run do
    spawn_and_send("fred")
    spawn_and_send("betty")
  end

  def spawn_and_send(token) do
    process = spawn(Processes, :mimic, [self])
    send process, token
    receive do
      response -> IO.puts inspect response
    end
  end

  def mimic(caller) do
    receive do
      token = "fred" -> 
        # This code is deterministic/we will always get the same result.
        # I tried to see if we could get betty before fred by making mimic
        # sleep for 1 second when "fred" is received as token.
        # :timer.sleep(1000) 
        send caller, token
      token -> send caller, token
    end
  end
end