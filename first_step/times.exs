defmodule Times do
    def double(n), do: n * 2
    def triple(n), do: n * 3
    def quadruple(n), do: 2 * double(n)
    def octople(n), do: 2 * quadeuple(n)
end