defmodule MyList do
    def len([]), do: 0
    def len([ head | tail ]), do: 1 + len(tail)

    def square([]), do: []
    def square([ h | t ]), do: [ h*h | square(t) ]

    def add_1([]), do: []
    def add_1([h|t]), do: [h + 1 | add_1(t)]

    def map([], _func), do: []
    def map([h|t], func), do: [func.(h) | map(t, func)]

    def sum(ls), do: _sum(ls, 0)

    defp _sum([], total),    do: total
    defp _sum([h|t], total), do: _sum(t, h+total)

    def sum_1([]), do: []
    def sum_1([h|t]), do: h + sum(t) 

    def reduce([], value, _), do: value
    def reduce([h|t], value, func) do
        reduce(t, func.(h, value), func)
    end

    def mapsum(ls, f), do: reduce(map(ls, f), 0, &(&1 + &2))

    def max(ls), do: reduce(ls, 0, fn (h,v) -> if h > v do h else v end end)

    def caesar([], _n), do: []
    def caesar([ head | tail ], n)
        when head+n <= ?z,
        do: [ head+n | caesar(tail, n) ]

    def caesar([ head | tail ], n), do: [ head+n-26 | caesar(tail, n) ]
    
    def span(from, to), do: _span(from + 1, to, [from])

    defp _span(from, to, temp_list) when from >= to, do: temp_list
    defp _span(from, to, temp_list), do: _span(from + 1, to, [from + 1 | temp_list] )

    def span_1(_from = to, to), do: [to] 
    def span_1(from, to) when from < to, do: [from | span_1(from + 1, to)]

    def span_2(from, to), do: Enum.to_list(from..to)

    def prime(n) do
        primes_list = Enum.filter(span_1(2, n), &(_is_prime?(&1)))
        List.last(primes_list) 
    end

    defp _is_prime?(2), do: true
    defp _is_prime?(n) do
        primes_list = Enum.all?(span_1(2, n - 1), &(rem(n, &1) != 0)) 
    end

    def prime_1(n) do
        !(for nx <- span_1(2, n), ny <- span_1(2, n), nx != ny, nx > ny, do: rem(nx, ny) == 0) |> Enum.any?  
    end

    
end

defmodule Swapper do
    def swap([]), do: []
    def swap([a, b | tail]), do: [ b, a | tail ]
    def swap([_]), do: raise "Can't swap a list with an odd number of elements" 
end

defmodule WeatherHistory do
    def for_location([], _target_loc), do: []
    def for_location( [ head = [ _, target_loc, _, _ ] | tail], target_loc) do
        [ head | for_location(tail, target_loc) ]
    end
    def for_location([ _ | tail ], target_loc), do: for_location(tail, target_loc)

    def test_data do
        [
            [1366225622, 27, 15, 0.125],
            [1366225622, 28, 15, 0.55],
            [1366225622, 26, 21, 0.25],
            [1366225622, 27, 19, 0.51],
            [1366225622, 28, 17, 0.13],
            [1366225622, 26, 15, 0.081],
            [1366225622, 28, 22, 0.468],
            [1366225622, 27, 21, 0.60],
            [1366225622, 28, 30, 0.059],
            [1366225622, 26, 14, 0.05],
            [1366225622, 27, 22, 0.13],
            [1366225622, 28, 17, 0.03],
            [1366225622, 26, 19, 0.025]
        ]
    end
end
