defmodule Spawn2 do
    def greet do
        receive do
            {sender, msg} ->
            send sender, { :ok, "Hello #{msg}"}
        end
    end
end

#here is a clinet
pid = spawn(Spawn2, :greet, [])

send pid, {self, "Woeld!"}

receive do
    {:ok, msg} ->
    # code
    IO.puts msg
end

send pid, {self, "Kermit!"}
receive do
    {:ok, msg} ->
    # code
    IO.puts msg
end

