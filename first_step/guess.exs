defmodule Chop do
    def guess(actual, a..b) do
        mid = mid(a..b)
        IO.puts "It is #{mid}"
        go actual, a..b, mid
    end

    defp go(actual, _.._, current) when actual == current do
        IO.puts "#{actual}"
    end

    defp go(actual, a.._, current) when actual < current do
        guess actual, a..current
    end

    defp go(actual, _..b, current) when actual > current do
        guess actual, current..b
    end


    defp mid(a..b) do
        a + div(b - a, 2)
    end
end