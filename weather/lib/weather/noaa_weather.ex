defmodule Weather.NoaaWeather do
    require Logger
    @noaa_url Application.get_env(:weather, :noaa_url)

    require Record
    Record.defrecord :xmlElement, Record.extract(:xmlElement, from_lib: "xmerl/include/xmerl.hrl")
    Record.defrecord :xmlText,    Record.extract(:xmlText,    from_lib: "xmerl/include/xmerl.hrl")

    @user_agent [{"User-Agent", "Jonatan Borkowski y2kjohny@gmial.com"}]

    def fetch(location) do
        Logger.info "Fetching weather for location name: #{location}"
        weather_url(location)
        |> HTTPoison.get(@user_agent)
        |> handle_response
    end

    def weather_url(location) do
        "#{@noaa_url}/#{String.upcase(location)}.xml"
    end

    def handle_response({ :ok, %{ status_code: 200, body: raw_body }}) do
        Logger.info "Successful response"
        # [ body ] = :xmerl_xpath.string('/current_observation', xml)
        body = parse(raw_body)
        # IO.puts body
        { :ok, body }
    end

    def handle_response({ :error, %{ status_code: status, body: error }}) do
        Logger.info "Error #{status} returned"
        { :error, error }
    end

    def parse(body) do
        body
        |> scan_body
        |> parse_xml
    end

    def scan_body(body) do
        # body |> :binary.bin_to_list |> :xmerl_scan.string
        body |> :unicode.characters_to_binary |> :binary.bin_to_list |> :xmerl_scan.string
    end

    def parse_xml({ xml, _ }) do
        #  [element]  = :xmerl_xpath.string('/current_observation/location', xml)
        #  [text]     = xmlElement(element, :content)
        #  value      = xmlText(text, :value)
        #  IO.inspect to_string(value)
        # => "Two of our famous Belgian Waffles with plenty of real maple syrup"

        # Enum.each(map, fn(i) ->
        #      IO.inspect i
        # end)
        list_of_all_to_map(xml, "//*")
    end

    def list_of_all_to_map(node, path) do
        xpath(node, path)
        |> Enum.map(fn v -> [ node_name(v), text(v) ] end)
        |> Enum.reduce(%{}, fn ([key, val], acc) -> Map.put(acc, key, val) end)
    end

    def node_name(nil), do: nil
    def node_name(node), do: elem(node, 1)

    def text(node), do: node |> xpath('./text()') |> extract_text
    defp extract_text([xmlText(value: value)]), do: List.to_string(value)
    defp extract_text(_x), do: nil

    defp xpath(nil, _), do: []
    defp xpath(node, path) do
        :xmerl_xpath.string(to_char_list(path), node)
    end
end