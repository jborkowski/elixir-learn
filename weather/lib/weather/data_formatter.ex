defmodule Weather.DataFormatter do
    import Enum, only: [ each: 2, map: 2 ]

    def print_data_to_table(values, headers) do
        with data = split_into_column(values, headers)
        do
            data
            |> each(fn [head | tail] -> IO.puts "#{head}: #{tail}" end)
        end
    end

    def split_into_column(values, headers) do
        map(headers, fn {k, v} -> [v, values[k]] end)
    end

    def printable(str) when is_binary(str), do: str
    def printable(str), do: to_string(str)
end