defmodule Weather.CLI do
    import Weather.DataFormatter, only: [ print_data_to_table: 2]

    def run(argv) do
        argv
        |> parse_args
        |> process
    end

    @doc """
    'argv' can be -h or --help, which returns :help.

    Otherwise it is a location code for noaa weather gov

    return a tuple of '{location}' or ':help' if help given
    """
    def parse_args(argv) do
        parse = OptionParser.parse(argv, switches: [ help: :boolean], aliases: [ h: :help ])
        case parse do
            {[ help: true ], _, _} -> :help
            {_, [location], _}   -> { location }
            _                   -> :help
        end
    end

    def process(:help) do
        IO.puts """
        usage: weather <location_code>
        hinit: find location code on page: http://w1.weather.gov/xml/current_obs/
        """
    end

    def process({location}) do
        Weather.NoaaWeather.fetch(location)
        |> decode_response
        |> print_data_to_table(%{:observation_time => "Observation time", :weather => "Weather", :temperature_string => "Temperature", :relative_humidity => "Relative humidity"})
    end

    def decode_response({:ok, body}), do: body
    
    def decode_response({:error, error}) do
        {_, message} = List.keyfind(error, "message", 0)
        IO.puts "Error fetching form Noaa: #{message}"
        System.halt(2)
    end
end