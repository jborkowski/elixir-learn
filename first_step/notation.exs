add_one = &(&1 + 1)
square = &(&1 + &1)

rnd = &(Float.round(&1, &2))

# Excercise 

IO.puts Enum.map [1,2,3,4], &(&1 + 2)

IO.puts Enum.each [1,2,3,4], &(IO.inspect &1)