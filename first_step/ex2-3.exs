check = fn
    (0,0,0) -> IO.puts("FizzBuzz")
    (0,_,_) -> IO.puts("Fizz")
    (_,0,_) -> IO.puts("Buzz")
    (_,_,a) -> IO.puts(a)
end

check_rem = fn (n) -> check.(rem(n,3), rem(n,5), n) end

IO.puts check.(0,0,0)

IO.puts check.(0,21,32)

IO.puts check.(123,0,123)

IO.puts check.(123,213,1222222)

IO.puts check_rem.(123)

prefix = fn pre -> (fn fix -> "#{pre} #{fix}" end) end 

IO.puts prefix.(123)



