defmodule Canvas do

  @defaults [ fg: "black", bg: "white", font: "Merriweather" ]

  def draw_text(text, options \\ []) do
    options = Keyword.merge(@defaults, options)
    IO.puts "Drawing text #{inspect(text)}"
    IO.puts "Foreground:  #{options[:fg]}"
    IO.puts "Background:  #{Keyword.get(options, :bg)}"
    IO.puts "Font:        #{Keyword.get(options, :font)}"
    IO.puts "Pattern:     #{Keyword.get(options, :pattern, "solid")}"
    IO.puts "Style:       #{inspect Keyword.get_values(options, :style)}"
  end

end

## Canvas.draw_text("hello", fg: "red", style: "italic", style: "bold")

people = [
    %{ name: "Grumpy",      height: 1.76 },
    %{ name: "Lucy",        height: 1.60 },
    %{ name: "Jonathan",    height: 1.23 },
    %{ name: "Dave",        height: 1.91 },
    %{ name: "Dopey",       height: 1.21 },
    %{ name: "Shaquille",   height: 1.32 },
    %{ name: "Sneezy",      height: 1.80 }
]    

IO.inspect(for person = %{ height: height } <- people, height > 1.5, do: person)

defmodule HotelRoom do
    def book(%{ name: name, height: height }) when height > 1.9 do
        IO.puts "Needs extra long bed for #{name}"
    end

    def book(%{ name: name, height: height }) when height < 1.3 do
        IO.puts "Needs low shower controls for #{name}"
    end

    def book(%{ name: name, height: height }) do
        IO.puts "Needs regular bad for #{name}"
    end


end

people |> Enum.each(&HotelRoom.book/1)

defmodule Subscriber do
    defstruct name: ".", paid: false, over_18: true
end

defmodule Attendee do
    defstruct name: ".", paid: false, over_18: true

    def may_attend_after_party(attendee = %Attendee{}) do
        attendee.paid && attendee.over_18
    end

    def print_vip_badge(%Attendee{name: name}) when name != "." do
        IO.puts "Very cheap badge for #{name}"
    end

    def print_vip_badge(%Attendee{}) do
        IO.puts "Missing name for badge"
    end
end