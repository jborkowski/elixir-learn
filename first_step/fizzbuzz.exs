defmodule Fizzbuzz do
    def upto(n) when n > 0 do
        1..n |> Enum.map(&fizzbuzz/1)
    end

    defp fizzbuzz(n) do
        cond do
            rem(n, 3) == 0 and rem(n, 5) == 0 ->
                "FizzBuzz"
            rem(n, 3) == 0 ->
                "Fizz"
            rem(n, 5) == 0 ->
                "Buzz"
            true -> 
                n    
        end
    end

    def upto_1(n) do
       1..n |> Enum.map(&_fizzbuzz/1)
    end

    defp _fizzbuzz(n), do: _fizzword(n, rem(n, 3), rem(n, 5))

    defp _fizzword(_n,0,0), do: "FizzBuzz"
    defp _fizzword(_n,_,0), do: "Fizz"
    defp _fizzword(_n,0,_), do: "Buzz"
    defp _fizzword(n,_,_), do: n

    def upto_1_case(n) do
       1..n |> Enum.map(&_fizzbuzz_case/1)
    end
    defp _fizzbuzz_case(n) do
        case {n, rem(n,3), rem(n,5)} do
             {_, 0, 0} -> "FizzBuzz"
             {_, _, 0} -> "Fizz"
             {_, 0, _} -> "Buzz"
             {n, _, _} -> n 
        end
    end
end