defmodule Spawn do
  import :timer, only: [ sleep: 1 ]

  def run do
    Process.flag(:trap_exit, true)
    _res = spawn_link(Spawn, :call_parent, [self])
    sleep(500)
    receive_message
  end

  def call_parent(parent) do
    send parent, "Boom!"
    exit "bye"
  end

  def receive_message do
    receive do
      msg ->
        IO.puts "#{inspect msg}"
      after 500 ->
        IO.puts "timeout"
    end
  end
end

Spawn.run