defmodule Spawn3 do
    def greet do
        receive do
            {sender, msg} ->
            send sender, { :ok, "Hello #{msg}"}
        end
    end
end

#here is a clinet
pid = spawn(Spawn3, :greet, [])

send pid, {self, "Woeld!"}

receive do
    {:ok, msg} ->
        IO.puts msg
    after 500 ->
        IO.puts "The greeter has gone away"
end

send pid, {self, "Kermit!"}
receive do
    {:ok, msg} ->
        IO.puts msg
    after 500 ->
        IO.puts "The greeter has gone away"
end

