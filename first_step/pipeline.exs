[1,2,3,4,5,6] 
|> Enum.map(&(&1*&1))
|> Enum.with_index
|> Enum.map(fn {value, index} -> value - index end)
|> IO.inspect 

IO.puts File.read!("/usr/share/dict/words")
|> String.split
|> Enum.max_by(&String.length/1)

IO.puts File.open!("/usr/share/dict/words")
|> IO.stream(:line)
|> Enum.max_by(&String.length/1)

## runs two time slower, but without intermediate result storage ;)
IO.puts File.stream!("/usr/share/dict/words")
|> Enum.max_by(&String.length/1)

Enum.map(1..10_000_000, &(&1+1)) |> Enum.take(5)
Stream.map(1..10_000_000, &(&1+1)) |> Enum.take(5)
