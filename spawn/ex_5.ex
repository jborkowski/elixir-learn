defmodule Spawn do
  import :timer, only: [ sleep: 1 ]

  def run do
    res = spawn_monitor(Spawn, :call_parent, [self])
    IO.inspect res
    sleep(500)
    receive_message
  end

  def call_parent(parent) do
    send parent, "Boom!"
    exit "bye"
    # raise RuntimeError, message: "bye"
  end

  def receive_message do
    receive do
      msg ->
        IO.puts "#{inspect msg}"
      after 500 ->
        IO.puts "timeout"
    end
  end
end

Spawn.run