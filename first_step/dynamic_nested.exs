nested = %{
    buttercup: %{
        actor: %{
            first: "Robin",
            last: "Wright"
        },
        role: "princess"
    },
    westley: %{
        actor: %{
            first: "Carey",
            last: "Ewes"
        },
        role: "farm boy"
    }
}

IO.inspect get_in(nested, [:buttercup])

IO.inspect get_in(nested, [:buttercup, :actor])

IO.inspect get_in(nested, [:buttercup, :actor, :first])

IO.inspect put_in(nested, [:westley, :actor, :last], "Elwes")

authors = [
    %{ name: "Jose", language: "Elixir" },
    %{ name: "Jacob", language: "Scala" },
    %{ name: "Jonatan", language: "Java" }
]

languages_with_an_r = fn (:get, collection, next_fn) ->
    for row <- collection do 
        if String.contains?(row.language, "a") do
            next_fn.(row)
        end
    end
end

IO.inspect get_in(authors, [languages_with_an_r, :name])